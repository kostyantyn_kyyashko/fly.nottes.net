<?php
namespace App\Entity;
class MongoManager3
{
    /**
     * @return \Doctrine\Bundle\MongoDBBundle\ManagerRegistry
     */
    private function getMongoService ()
    {
        global $kernel;
        return $kernel->getContainer()->get('doctrine_mongodb');
    }

    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    public function createManager ()
    {
        return $this->getMongoService()->getManager();
    }
}