<?php
namespace App\Document;
class MongoBase
{
    public function __construct()
    {
        $this->manager = MongoManager::getInstance()->createManager();
    }

    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    public $manager;

    /**
     * insert row to the "table"
     * array keys must comply property names of class
     * @param array $fieldValues [field1 => value1, field2 => value2, ...]
     * @return bool
     * @throws \Exception
     */
    public function insertRow(array $fieldValues)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        $builder->insert();
        foreach ($fieldValues as $field => $value) {
            $builder
                ->field($field)->set($value);
        }
        try {
            $builder
                ->getQuery()
                ->execute();
            return true;
        }
        catch (\Exception $e)
        {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * @param array $fieldsValuesConditions [field1 => value1, field2 => value2, ...], all collection if empty
     * @param array $fieldsForSelect [field1, field2...], all fields if empty
     * @return mixed
     */
    public function selectBy(array $fieldsValuesConditions = [], array $fieldsForSelect = [])
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        $builder->hydrate(false);
        foreach ($fieldsValuesConditions as $field => $value) {
            $builder
                ->field($field)->equals($value);
        }
        if (count($fieldsForSelect) > 0) {
            $builder
                ->select($fieldsForSelect);
        }
        try {
            $result = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            echo $e->getMessage();
            die();
        }
        return $result;
    }

    /**
     * @param array $fieldsValuesConditions
     */
    public function remove(array $fieldsValuesConditions = [])
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        $builder->remove();
        foreach ($fieldsValuesConditions as $field => $value) {
            $builder
                ->field($field)->equals($value);
        }
        try {
            $builder
                ->getQuery()
                ->execute();
        } catch (\Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    public function updateOne(array $fieldsValuesConditions = [], $fieldsNewValues = [])
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        $builder->updateOne();
        foreach ($fieldsValuesConditions as $field => $value) {
            $builder
                ->field($field)->equals($value);
        }
        foreach ($fieldsNewValues as $f => $v) {
            $builder
                ->field($f)->set($v);
        }
        try {
            $builder
                ->getQuery()
                ->execute();
        } catch (\Exception $e) {
            echo $e->getMessage();
            die();
        }


    }
}
