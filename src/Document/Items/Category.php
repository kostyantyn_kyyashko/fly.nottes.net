<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items;

use App\Document\MongoBase;
use App\Document\MongoManager;
use App\Document\Utils;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Category
 * @package App\Document\Items
 * @MongoDB\Document(db="obt")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"categoryId"="asc"}, unique=true),
 *     @MongoDB\Index(keys={"categoryLevel"="asc"}),
 *     @MongoDB\Index(keys={"categoryParent"="asc"}),
 *     @MongoDB\Index(keys={"categoryName"="asc"})
 *     })
 *
 */
class Category extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $categoryId;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $categoryLevel;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $categoryParent;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $categoryName;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $categoryFileName;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCategoryId(): string
    {
        return $this->categoryLevel;
    }

    /**
     * @param string $categoryId
     */
    public function setCategoryId(string $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return string
     */
    public function getCategoryLevel(): string
    {
        return $this->categoryLevel;
    }

    /**
     * @param string $categoryLevel
     */
    public function setCategoryLevel(string $categoryLevel): void
    {
        $this->categoryLevel = $categoryLevel;
    }

    /**
     * @return string
     */
    public function getCategoryParent(): string
    {
        return $this->categoryParent;
    }

    /**
     * @param string $categoryParent
     */
    public function setCategoryParent(string $categoryParent): void
    {
        $this->categoryParent = $categoryParent;
    }

    /**
     * @return string
     */
    public function getCategoryName(): string
    {
        return $this->categoryName;
    }

    /**
     * @param string $categoryName
     */
    public function setCategoryName(string $categoryName): void
    {
        $this->categoryName = $categoryName;
    }

    /**
     * @return string
     */
    public function getCategoryFileName(): string
    {
        return $this->categoryFileName;
    }

    /**
     * @param string $categoryFileName
     */
    public function setCategoryFileName(string $categoryFileName): void
    {
        $this->categoryFileName = $categoryFileName;
    }

    /**
     * @return array
     */
    public function getTree() {
        $builder = MongoManager::getInstance()->createManager()
            ->createQueryBuilder(self::class);
        try {
            $dataset = $builder
                ->hydrate(false)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }

        $tree = [];
        $treeOut = [];
        foreach ($dataset as $category) {
            $treeOut[$category['categoryId']] = $category;
        }
        foreach ($treeOut as $id => &$node) {
            //Если нет вложений
            if (!isset($node['categoryParent']) || !$node['categoryParent']){
                $tree[$id] = &$node;
            }else{
                //Если есть потомки то перебераем массив
                $treeOut[$node['categoryParent']]['childs'][$id] = &$node;
            }
        }
        return $tree;
    }

    /**
     * get categories level 0
     * @return array
     */
    public function getRootCategories() {
        $builder = MongoManager::getInstance()->createManager()
            ->createQueryBuilder(self::class);
        try {
            $dataset = $builder
                ->hydrate(false)
                ->field('categoryLevel')->equals(0)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
        $out = [];
        foreach ($dataset as $id => $data) {
            $data['_id'] = $id;
            $out[] = $data;
        }
        return $out;
    }

    /**
     * @param $level
     * @return mixed
     */
    public function getParentCategoriesByLevel($level)
    {
        $builder = MongoManager::getInstance()->createManager()
            ->createQueryBuilder(self::class);
        try {
            $parents = $builder
                ->hydrate(false)
                ->field('categoryLevel')->equals($level)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
        return $parents;
    }

    /**
     * @var array
     */
    private $cfc = [];

    /**
     * @param array $tree
     * @return array
     */
    public function getChildFreeCategories(array $tree)
    {
        foreach ($tree as $id => $t) {
            if (!isset($t['childs'])) {
                $this->cfc[] = $t;
            }
            else {
                static::getChildFreeCategories($t['childs']);
            }
        }
        $out = [];
        $counter = 0;
        foreach ($this->cfc as $data) {
            $data['_id'] = $data['_id']->__toString();
            $out[] = $data;
            $counter++;
        }
        return $out;
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    public function getCategoryByCategoryId($categoryId)
    {
        $builder = MongoManager::getInstance()->createManager()
            ->createQueryBuilder(self::class);
        try {
            $category = $builder
                ->hydrate(false)
                ->field('categoryId')->equals($categoryId)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
        $category = isset(array_values($category)[0])?array_values($category)[0]:false;
        return $category;
    }

    /**
     * @param $categoryName
     * @return mixed
     */
    public function getCategoryByName($categoryName)
    {
        $builder = MongoManager::getInstance()->createManager()
            ->createQueryBuilder(self::class);
        try {
            $category = $builder
                ->hydrate(false)
                ->field('categoryName')->equals($categoryName)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
        $category = isset(array_values($category)[0])?array_values($category)[0]:false;
        return $category;
    }

    /**
     * @param $_id
     * @param $categoryName
     */
    public function saveCategoryNameById($_id, $categoryName)
    {
        $builder = MongoManager::getInstance()->createManager()
            ->createQueryBuilder(self::class);
        try {
            $builder
                ->updateOne()
                ->field('_id')->equals($_id)
                ->field('categoryName')->set($categoryName)
                ->getQuery()
                ->execute();
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param $_id
     * @param $filename
     */
    public function saveCategoryFile($_id, $filename)
    {
        $builder = MongoManager::getInstance()->createManager()
            ->createQueryBuilder(self::class);
        try {
            $builder
                ->updateOne()
                ->field('_id')->equals($_id)
                ->field('categoryFileName')->set($filename)
                ->getQuery()
                ->execute();
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param $_id
     */
    public function deleteCategoryById($_id)
    {
        $builder = $this->manager->createQueryBuilder(self::class);
        try {
            $builder
                ->remove()
                ->field('_id')->equals($_id)
                ->getQuery()
                ->execute();
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }

    private $root;
    public function getRootCategory($categoryId)
    {
        $cat = $this->getCategoryByCategoryId($categoryId);
        $level = $cat['categoryLevel'];
        $this->root = $cat['categoryName'];
        if ($level > 0) {
            $this->getRootCategory($cat['categoryParent']);
        }
        return $this->root;
    }

    public function getRootCategoryByName($categoryName)
    {
        $cat = $this->getCategoryByName($categoryName);
        $level = $cat['categoryLevel'];
        $this->root = $cat['categoryName'];
        if ($level > 0) {
            $this->getRootCategory($cat['categoryParent']);
        }
        return $this->root;
    }

    /**
     * @param $parentCategoryId
     * @return array
     */
    public function getCategoriesByParentCategory($parentCategoryId)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        $categories = [];
        try {
            $categories = $builder
                ->hydrate(false)
                ->field('categoryParent')->equals($parentCategoryId)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $exception)
        {
            Utils::log($exception->getMessage());
        }
        return $categories;
    }
}
