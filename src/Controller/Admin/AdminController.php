<?php
namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AdminController extends AbstractController
{

    /**
     * @Route("/admin")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $html = $this->renderView('admin/common/page_header.html.twig');
        $items = [
            [
                'href'=>'#',
                'msg' => 'Orders'
            ],
            [
                'href'=>'#',
                'msg' => 'Peoples'
            ],
            [
                'href'=>'#',
                'msg' => 'Analytics'
            ],
            [
                'href'=>'#',
                'msg' => 'Any from array [url, name]'
            ],
        ];
        $content = "<h1>Content here</h1>";
        $html .= $this->renderView('admin/blank_page.html.twig', ['items' => $items, 'content' => $content]);
        $html .= $this->renderView('admin/common/page_footer.html.twig');
        return new Response($html);
    }

    /**
     * @Route("/ltedemo")
     * @param Request $request
     * @return Response
     */
    public function demo(Request $request)
    {
        $html = $this->renderView('admin/common/page_header.html.twig');
        $items = [
            [
                'href'=>'#',
                'msg' => 'Orders'
            ],
            [
                'href'=>'#',
                'msg' => 'Peoples'
            ],
            [
                'href'=>'#',
                'msg' => 'Analytics'
            ],
            [
                'href'=>'#',
                'msg' => 'Any from array [url, name]'
            ],
        ];
        $content = "<h1>Content here</h1>";
        $html .= $this->renderView('admin/blank_page2.html.twig', ['items' => $items, 'content' => $content]);
        $html .= $this->renderView('admin/common/page_footer.html.twig');
        return new Response($html);
    }

}