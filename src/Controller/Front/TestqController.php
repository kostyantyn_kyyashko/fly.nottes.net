<?php
namespace App\Controller\Front;

use App\Document\Items\Category;
use App\Document\MongoBase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestqController extends AbstractController
{
    /**
     * @Route("/testq")
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function testq1(Category $category)
    {
        $category->insertRow(['slon' => 'yoooo']);
        return new Response('ok');
    }
}