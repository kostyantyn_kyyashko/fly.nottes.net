<?php
namespace App\Controller\Front;

use App\Document\MongoBase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{
    /**
     * @Route("/")
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function demo(Request $request)
    {
        $html = $this->renderView('front/common/page_header.html.twig');
        $html .= $this->renderView('front/index/index.html.twig');
        $html .= $this->renderView('front/common/page_footer.html.twig');
        return new Response($html);
    }

    /**
     * @Route("/cart")
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function cart(Request $request)
    {
        $html = $this->renderView('front/common/page_header.html.twig');
        $html .= $this->renderView('front/cart/cart.html.twig');
        $html .= $this->renderView('front/common/page_footer.html.twig');
        return new Response($html);
    }

    /**
     * @Route("/contact")
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function contact(Request $request)
    {
        $html = $this->renderView('front/common/page_header.html.twig');
        $html .= $this->renderView('front/contact/contact.html.twig');
        $html .= $this->renderView('front/common/page_footer.html.twig');
        return new Response($html);
    }

    /**
     * @Route("/testq")
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function testq(Request $request)
    {
        $mb = new MongoBase();
        return new Response('');
    }

}